#pragma once

#include <thread>
#include <mutex>

#ifdef _WIN32 
#include <opencv/cv.hpp>
#elif __linux__
#include <opencv2/cvv.hpp>
#endif
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"

#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartGlobal>
#include <qtimer.h>
#include "ui_HealthCamDataCollector.h"

#include "PulseOximeterInterface.h"
#include "threadpool.hpp"
#include "CsvWriter.h"

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QChart;
class QChartView;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class HealthCamDataCollector : public QMainWindow
{
    Q_OBJECT
public slots:
	void DisplayImage();

public:
    HealthCamDataCollector(QWidget *parent = Q_NULLPTR);
	

private:
	// variables
    Ui::HealthCamDataCollectorClass ui;
	cv::VideoCapture *camera;

	PulseOximeterInterface *pulseOxim;

	std::thread *pulseOxDisplayThread;
	bool runPulseOxDisplayThread;

	QImage imdisplay;
	QTimer* Timer;
	QChart *m_chart;
	QLineSeries *m_series;
	QChartView *chartView;
	std::mutex dataAccessMutex;

	bool startRecording;
	std::string saveSuffix;
	std::string saveImageDir;
	CSVWriter *pulseOxCsvWriter;
	ThreadPool *threadPool;

	// methods
	void CreateChart();
	void PulseOxDisplayFunc();
	void HandleButton();

protected:
	void closeEvent(QCloseEvent *event);
};
