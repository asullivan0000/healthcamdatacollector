#include "HealthCamDataCollector.h"

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>
#include <QtCharts/QValueAxis>
#include <QMutex>
#include <chrono>
#include <time.h>
#include <ctime>
#include <iostream>     // std::cout, std::ios
#include <sstream>      // std::ostringstream

#ifdef _WIN32
#include <direct.h>
#elif __linux__
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#endif

HealthCamDataCollector::HealthCamDataCollector(QWidget *parent)
    : QMainWindow(parent),
	m_chart(new QChart),
	m_series(new QLineSeries),
	startRecording(false),
	saveSuffix("Data/"),
	saveImageDir("Data/"),
	pulseOxCsvWriter(nullptr)
{
    ui.setupUi(this);
	const QRect rect = this->geometry();
	this->setGeometry(100,100, 1600, 500);

	CreateChart();
	ui.verticalLayout->addWidget(chartView);

	connect(ui.recordButton, &QPushButton::released, this, &HealthCamDataCollector::HandleButton);

	runPulseOxDisplayThread = true;
	pulseOxDisplayThread = new std::thread(&HealthCamDataCollector::PulseOxDisplayFunc, this);

	// open the first webcam plugged in the computer
	camera = new cv::VideoCapture(0); //"nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)640, height=(int)480,format=(string)NV12, framerate=(fraction)30/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! appsink");
	if (!camera->isOpened()) {
		std::cerr << "ERROR: Could not open camera" << std::endl;
		return;
	}

    camera->set(cv::CAP_PROP_FRAME_WIDTH, 640);
    camera->set(cv::CAP_PROP_FRAME_HEIGHT, 480);   
                                                                                                                                              
	threadPool = new ThreadPool();

	Timer = new QTimer(this);
	connect(Timer, SIGNAL(timeout()), this, SLOT(DisplayImage()));
	Timer->start();
}

void HealthCamDataCollector::closeEvent(QCloseEvent *event)
{
	runPulseOxDisplayThread = false;
	pulseOxim->Shutdown();

	if (pulseOxCsvWriter)
		delete pulseOxCsvWriter;

	if (threadPool)
		delete threadPool;

}

void HealthCamDataCollector::HandleButton()
{
	if (startRecording)
	{
		ui.recordButton->setText("Start Recording");
		ui.recordButton->setStyleSheet("QPushButton {color: black;}");
		startRecording = false;
		pulseOxCsvWriter->close();
		delete pulseOxCsvWriter;
		pulseOxCsvWriter = nullptr;
	}
	else
	{
		ui.recordButton->setText("Stop Recording");
		ui.recordButton->setStyleSheet("QPushButton {color: red;}");
		startRecording = true;

        std::ostringstream dir;

#if defined(WIN32) || defined(_WIN32)
		SYSTEMTIME now;
		GetLocalTime(&now);
	
		dir << "_" << std::setfill('0') << std::setw(4) << now.wYear << std::setfill('0') << std::setw(2) << now.wMonth << std::setfill('0') << std::setw(2) << now.wDay;
		dir << "_" << std::setfill('0') << std::setw(2) << now.wHour << std::setfill('0') << std::setw(2) << now.wMinute << std::setfill('0') << std::setw(2) << now.wSecond;
		
#elif __linux__
        time_t rawtime;
        struct tm * now;

        time (&rawtime);
        now = localtime (&rawtime);

        dir << "_" << std::setfill('0') << std::setw(4) << (now->tm_year + 1900) << std::setfill('0') << std::setw(2) << (now->tm_mon + 1) << std::setfill('0') << std::setw(2) << now->tm_mday;
		dir << "_" << std::setfill('0') << std::setw(2) << now->tm_hour << std::setfill('0') << std::setw(2) << now->tm_min << std::setfill('0') << std::setw(2) << now->tm_sec;
#endif
        saveSuffix = dir.str();
		saveImageDir = "Data/Images" + saveSuffix + "/";

#if defined(WIN32) || defined(_WIN32)
		mkdir(saveImageDir.c_str());
#elif __linux__
		// create a new directory for the image files
        mode_t mode = 0755;
		mkdir(saveImageDir.c_str(), mode);
#endif

		// create a CSVWriter to write the pulse ox data to a csv file
		std::ofstream *file = new std::ofstream("Data/PulseOx_" + saveSuffix + ".csv");
		pulseOxCsvWriter = new CSVWriter(file);
		pulseOxCsvWriter->newRow() << "time" << "beep" << "pulseWaveform" << "pulseRate" << "bloodSpO2";
	}
}

void HealthCamDataCollector::CreateChart()
{
	chartView = new QChartView(m_chart);
	chartView->setMinimumSize(600, 400);
	m_chart->addSeries(m_series);

	QValueAxis *axisX = new QValueAxis;
	axisX->setRange(0, 200);
	axisX->setLabelFormat("%g");
	axisX->setTitleText("Samples");

	QValueAxis *axisY = new QValueAxis;
	axisY->setRange(0, 1);
	axisY->setTitleText("Blood Sp02");

	m_chart->addAxis(axisX, Qt::AlignBottom);
	m_series->attachAxis(axisX);
	m_chart->addAxis(axisY, Qt::AlignLeft);
	m_series->attachAxis(axisY);
	m_chart->legend()->hide();
	m_chart->setTitle("Pulse Oxymeter Data");

	for (int i = 0; i < 200; i++)
		m_series->append(QPointF(i, 0.0));
}

void HealthCamDataCollector::PulseOxDisplayFunc()
{
	pulseOxim = new PulseOximeterInterface();

	int index = 0;
#ifdef _WIN32
	Sleep(100);
#elif __linux__
    usleep(100000);
#endif

	while (runPulseOxDisplayThread)
	{
		while (pulseOxim->howMuchData() == 0)
#ifdef _WIN32
	        Sleep(1);
#elif __linux__
            usleep(1000);
#endif

		while (pulseOxim->howMuchData() > 0) {
			PulseOxData data = pulseOxim->getData();

			m_series->replace(index, QPointF(index, data.pulseWaveform / 255.0f));
			ui.labelPulseRate->setText("Pulse rate: " + QString(std::to_string(int(data.pulseRate)).c_str()) + " bpm");
			ui.labelPulseOx->setText("Blood oxygenation: " + QString(std::to_string(int(data.bloodSpO2)).c_str()) + "%");
			
			if (startRecording &&  pulseOxCsvWriter)
			{
                std::ostringstream currentTime;
#ifdef _WIN32
				SYSTEMTIME now;
				GetLocalTime(&now);
		
				currentTime << std::setfill('0') << std::setw(2) << now.wHour << std::setfill('0') << std::setw(2) << now.wMinute << std::setfill('0') << std::setw(2) << now.wSecond;
                currentTime << "." << std::setfill('0') << std::setw(3) << now.wMilliseconds;
#elif __linux__
                time_t rawtime;
                struct tm * now;

                time (&rawtime);
                now = localtime (&rawtime);

                struct timeval start;
                gettimeofday(&start, NULL);

		        currentTime << std::setfill('0') << std::setw(2) << now->tm_hour << std::setfill('0') << std::setw(2) << now->tm_min << std::setfill('0') << std::setw(2) << now->tm_sec;
                currentTime << "." << std::setfill('0') << std::setw(3) << int(start.tv_usec / 1000.0);
#endif
				pulseOxCsvWriter->newRow() << currentTime.str() << std::to_string(int(data.beep)) << std::to_string(int(data.pulseWaveform)) << std::to_string(int(data.pulseRate)) << std::to_string(int(data.bloodSpO2));
			}

			index++;
			if (index >= 200)
			{
				index = 0;
#ifdef _WIN32
	            Sleep(16);
#elif __linux__
                usleep(16000);
#endif
			}
		}

#ifdef _WIN32
	    Sleep(10);
#elif __linux__
        usleep(10000);
#endif
	}
}

void HealthCamDataCollector::DisplayImage() 
{
	cv::Mat img;
	*camera >> img;
	
	cv::Mat imgout;
	cv::cvtColor(img, imgout, cv::COLOR_BGR2RGB); //Qt reads in RGB whereas CV in BGR
	QImage imdisplay((uchar*)imgout.data, imgout.cols, imgout.rows, imgout.step, QImage::Format_RGB888); //Converts the CV image into Qt standard format

	const QRect r = ui.display_image->geometry(); // Don't want to override the UI layout completely
	ui.display_image->setGeometry(r.x(), r.y(), imgout.cols, imgout.rows);
	ui.display_image->setPixmap(QPixmap::fromImage(imdisplay));//display the image in label that is created earlier

	//SYSTEMTIME now;
	//GetLocalTime(&now);

	std::ostringstream filename;
	filename << saveImageDir;

#ifdef _WIN32
	SYSTEMTIME now;
	GetLocalTime(&now);
		
	filename << "Image_" << std::setfill('0') << std::setw(4) << now.wYear << std::setfill('0') << std::setw(2) << now.wMonth << std::setfill('0') << std::setw(2) << now.wDay;
	filename << "_" << std::setfill('0') << std::setw(2) << now.wHour << std::setfill('0') << std::setw(2) << now.wMinute << std::setfill('0') << std::setw(2) << now.wSecond;
	filename << "." << std::setfill('0') << std::setw(3) << now.wMilliseconds << ".bmp";
#elif __linux__
    time_t rawtime;
    struct tm * now;

    time (&rawtime);
    now = localtime (&rawtime);

    struct timeval start;
    gettimeofday(&start, NULL);

    filename << "Image_" << std::setfill('0') << std::setw(4) << now->tm_year << std::setfill('0') << std::setw(2) << now->tm_mon << std::setfill('0') << std::setw(2) << now->tm_mday;
    filename << "_" << std::setfill('0') << std::setw(2) << now->tm_hour << std::setfill('0') << std::setw(2) << now->tm_min << std::setfill('0') << std::setw(2) << now->tm_sec;
    filename << "." << std::setfill('0') << std::setw(3) << (start.tv_usec * 1000.0) << ".bmp";
#endif

	if (startRecording)
		threadPool->queueWork(img, filename.str());
}
