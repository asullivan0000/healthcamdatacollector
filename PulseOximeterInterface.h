#pragma once
#include <vector>
#include <thread>
#include <deque>
#include <mutex>

#include "Serial.h"

struct PulseOxData {
	unsigned char signalStrenth;
	unsigned char fingerOut;
	unsigned char droppingSpO2;
	unsigned char beep;
	unsigned char pulseWaveform;
	unsigned char pulseRate;
	unsigned char bloodSpO2;

	PulseOxData()
	{
		signalStrenth = 0;
		fingerOut = 0;
		droppingSpO2 = 0;
		beep = 0;
		pulseWaveform = 0;
		pulseRate = 0;
		bloodSpO2 = 0;
	}
};

class PulseOximeterInterface
{
public:
	PulseOximeterInterface();
	~PulseOximeterInterface();

	int howMuchData();
	PulseOxData getData();
	void Shutdown() { run = false; };

private:
	Serial *serial;
	std::thread *acqThread;
	bool run;
	int packetCtr;
	std::deque<PulseOxData> dataQueue;
	std::mutex dataQueueMutex;

	bool InitializeCMS50D();
	void RespondToCMS50D();
	void AcqFunc();
};

