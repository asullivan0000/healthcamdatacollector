#include "PulseOximeterInterface.h"
#include <codecvt>
#include <cstring>

PulseOximeterInterface::PulseOximeterInterface() :
	packetCtr(0)
{
	int comPort = 1;
	bool fail = true;
#ifdef _WIN32
	while (fail)
	{
		std::string port = "COM" + std::to_string(comPort);
		serial = new Serial(port.c_str(), 115200);
		if (serial->isConnected())
		{
			fail = false;
			printf("connected on com%d\n", comPort);
			break;
		}
		else {
			printf("no device on com%d\n", comPort);
			if (comPort < 9)
				comPort++;
			else
				break;
		}
	}
#elif __linux__
    serial = new Serial("/dev/ttyUSB0", 115200);
	if (serial->isConnected())
	{
		fail = false;
		printf("connected on /dev/ttyUSB0\n");
	}
#endif

	if (fail)
		printf("Couldn't find Pulse Oxymeter on any COM port. Is it connected?\n");

	if (!fail)
	{
		while (!InitializeCMS50D())
		{
			printf("Trying to initialize Pulse Oxymeter. Is it turned on?\n");
#ifdef _WIN32
	        Sleep(100);
#elif __linux__
            usleep(100000);
#endif
		}

        printf("Initialization finished. starting thread.\m");
		run = true;
		acqThread = new std::thread(&PulseOximeterInterface::AcqFunc, this);
	}
}

PulseOximeterInterface::~PulseOximeterInterface()
{
	run = false;
}

bool PulseOximeterInterface::InitializeCMS50D()
{
    char initData[] = { 0x7D, 0x81, 0xA7, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xA2, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xA0, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xB0, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xAC, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xB3, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xA8, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xAA, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xA9, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
		0x7D, 0x81, 0xA1, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00 };

	serial->wwrite(initData, 90);

#ifdef _WIN32
	    Sleep(20);
#elif __linux__
        usleep(20000);
#endif

    int numReady = serial->in_waiting();

    if (numReady == 0)
        return false;
    
	char buf[5120];
    memset(&buf, '\0', 5120);
	int numRead = serial->rread(buf, numReady, false);
	printf("init numread: %d\n", numRead);

	if (numRead == 0)
		return false;

	return true;
}

void PulseOximeterInterface::RespondToCMS50D()
{
	char data[] = { 0x7D, 0x81, 0xAF, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00 };
	serial->wwrite(data);
}

void PulseOximeterInterface::AcqFunc()
{
    printf("thread started.\n");

	char buf[512];
	//int numRead = serial->rread(buf, 512);
	serial->flush();

	while (run) {
		int numAvail = serial->in_waiting();

		while (numAvail < 9)
		{
        	numAvail = serial->in_waiting();
#ifdef _WIN32
	        Sleep(1);
#elif __linux__
            usleep(1000);
#endif
        }            

		int numRead = serial->rread(buf, 20);

		if (numRead == 0)
			continue;	

#ifdef _WIN32
		if (!((buf[0] == 0x01) && (buf[8] == -1)))
#elif __linux__
		if (!((buf[0] == 0x01) && (buf[8] == 0xff)))
#endif	
			continue;

		packetCtr++;
		if (packetCtr >= 300)
			RespondToCMS50D();

		// There are 9 bytes in each data sample.
		// Bytes 0 and 1 are a header equal to 0x01, 0xe0.
		// Bytes 7 and 8 are a footer equal to 0xff, 0xff.
		PulseOxData data;
		data.signalStrenth = buf[2] & 0x0f;
		data.fingerOut = buf[2] & 0x10;
		data.droppingSpO2 = buf[2] & 0x20;
		data.beep = buf[2] & 0x40;
		data.pulseWaveform = buf[3];
		data.pulseRate = ((buf[4] & 0x40) << 1) | (buf[5] & 0x7f);
		data.bloodSpO2 = buf[6] & 0x7f;

		if (data.pulseWaveform < 64) {
			printf("%d\n", data.pulseWaveform);
			continue;
		}

		dataQueueMutex.lock();
		dataQueue.push_back(data);
		dataQueueMutex.unlock();

		//serial->flush();

		// Data is transmitted at 60Hz (16.67 msec)
#ifdef _WIN32
	    Sleep(12);
#elif __linux__
        usleep(12000);
#endif
	}
}

int PulseOximeterInterface::howMuchData()
{
	return dataQueue.size();
}

PulseOxData PulseOximeterInterface::getData()
{
	dataQueueMutex.lock();
	if (dataQueue.size() == 0)	
		return PulseOxData();

	auto temp = *dataQueue.begin();
	
	if (dataQueue.size() > 0)
		dataQueue.pop_front();

	dataQueueMutex.unlock();

	return temp;
}
