/** Serial.cpp
 *
 * A very simple serial port control class that does NOT require MFC/AFX.
 *
 * @author Hans de Ruiter
 *
 * @version 0.1 -- 28 October 2008
 */

#include <iostream>
#include <codecvt>

#ifdef __linux__
#include <cstring>
#endif

using namespace std;

#include "Serial.h"

Serial::Serial(const char *portName, int bitRate)
{
#if defined(WIN32) || defined(_WIN32)
	tstring commPortName = std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(portName);
	commHandle = CreateFile(commPortName.c_str(), GENERIC_READ|GENERIC_WRITE, 0,NULL, OPEN_EXISTING, 
		0, NULL);

	if(commHandle == INVALID_HANDLE_VALUE) 
	{
		return;// throw("ERROR: Could not open com port");
	}
	else 
	{
		// set timeouts
		COMMTIMEOUTS cto = { MAXDWORD, 0, 0, 0, 0};
		DCB dcb;
		if(!SetCommTimeouts(commHandle,&cto))
		{
			Serial::~Serial();
			throw("ERROR: Could not set com port time-outs");
		}

		// set DCB
		memset(&dcb,0,sizeof(dcb));
		dcb.DCBlength = sizeof(dcb);
		dcb.BaudRate = bitRate;
		dcb.fBinary = 1;
		dcb.fDtrControl = DTR_CONTROL_ENABLE;
		dcb.fRtsControl = RTS_CONTROL_ENABLE;

		dcb.Parity = NOPARITY;
		dcb.StopBits = ONESTOPBIT;
		dcb.ByteSize = 8;

		if(!SetCommState(commHandle,&dcb))
		{
			Serial::~Serial();
			throw("ERROR: Could not set com port parameters");
		}
	}
#elif __linux__
	serial_port = open(portName, O_RDWR);

	// Check for errors
	if (serial_port < 0) {
		printf("Error %i from open: %s\n", errno, strerror(errno));
		return;
	}

	struct termios tty;

	// Read in existing settings, and handle any error
	// NOTE: This is important! POSIX states that the struct passed to tcsetattr()
	// must have been initialized with a call to tcgetattr() overwise behaviour
	// is undefined
	if (tcgetattr(serial_port, &tty) != 0) {
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
		serial_port = -1;
		return;
	}

	
    tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
    tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
    tty.c_cflag |= CS8; // 8 bits per byte (most common)
    tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

    tty.c_lflag &= ~ICANON;
    tty.c_lflag &= ~ECHO; // Disable echo
    tty.c_lflag &= ~ECHOE; // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

    tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
    tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
    // tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
    // tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

    tty.c_cc[VTIME] = 0;
    tty.c_cc[VMIN] = 0;

    cfsetospeed(&tty, B115200);
    cfsetispeed(&tty, B115200);

	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	}
#endif
}

Serial::~Serial()
{
#if defined(WIN32) || defined(_WIN32)
	CloseHandle(commHandle);
#elif __linux__
	close(serial_port);
#endif
}

int Serial::wwrite(const char *buffer)
{
#if defined(WIN32) || defined(_WIN32)
	DWORD numWritten;
	WriteFile(commHandle, buffer, strlen(buffer), &numWritten, NULL); 

	return numWritten;
#elif __linux__
	int n = write(serial_port, buffer, strlen(buffer));
	return n;
#endif
}

int Serial::wwrite(const char *buffer, int buffLen)
{
#if defined(WIN32) || defined(_WIN32)
	DWORD numWritten;
	WriteFile(commHandle, buffer, buffLen, &numWritten, NULL); 

	return numWritten;
#elif __linux__
	int n = write(serial_port, buffer, buffLen);
	return n;
#endif
}

int Serial::in_waiting()
{
#if defined(WIN32) || defined(_WIN32)
	DWORD temp;
	COMSTAT ComState;
	ClearCommError(commHandle, &temp, &ComState);
	return ComState.cbInQue;
#elif __linux__
	int bytes_avail;
	ioctl(serial_port, FIONREAD, &bytes_avail);
    return bytes_avail;
#endif
}

int Serial::rread(char *buffer, int buffLen, bool nullTerminate)
{
#if defined(WIN32) || defined(_WIN32)
	DWORD numRead;
	if(nullTerminate)
	{
		--buffLen;
	}

	BOOL ret = ReadFile(commHandle, buffer, buffLen, &numRead, NULL);

	if(!ret)
	{
		return 0;
	}

	if(nullTerminate)
	{
		buffer[numRead] = '\0';
	}

	return numRead;
#elif __linux__
	if (nullTerminate)
	{
		--buffLen;
	}

	int n = read(serial_port, buffer, buffLen);

	if (nullTerminate)
	{
		buffer[n] = '\0';
	}

	return n;
#endif
}

#define FLUSH_BUFFSIZE 10

void Serial::flush()
{
	char buffer[FLUSH_BUFFSIZE];
	int numBytes = Serial::rread(buffer, FLUSH_BUFFSIZE, false);
	while(numBytes != 0)
	{
		numBytes = Serial::rread(buffer, FLUSH_BUFFSIZE, false);
	}
}
