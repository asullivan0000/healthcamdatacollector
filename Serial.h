/** Serial.h
 *
 * A very simple serial port control class that does NOT require MFC/AFX.
 *
 * License: This source code can be used and/or modified without restrictions.
 * It is provided as is and the author disclaims all warranties, expressed 
 * or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * @author Hans de Ruiter
 *
 * @version 0.1 -- 28 October 2008
 */
#pragma once

#include <string>
#if defined(WIN32) || defined(_WIN32)
#include <windows.h>
typedef std::basic_string<TCHAR> tstring;
#elif __linux__
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <sys/ioctl.h>
#endif


class Serial
{
private:
#if defined(WIN32) || defined(_WIN32) 
    HANDLE commHandle;
#elif __linux__
    int serial_port;
#endif

public:
    Serial(const char *commPortName, int bitRate = 115200);

    virtual ~Serial();

    bool isConnected() {
#if defined(WIN32) || defined(_WIN32) 
	    return commHandle != INVALID_HANDLE_VALUE;
#elif __linux__
	    return serial_port > 0;
#endif
    };

    /** Writes a NULL terminated string.
     *
     * @param buffer the string to send
     *
     * @return int the number of characters written
     */
    int wwrite(const char buffer[]);

    /** Writes a string of bytes to the serial port.
     *
     * @param buffer pointer to the buffer containing the bytes
     * @param buffLen the number of bytes in the buffer
     *
     * @return int the number of bytes written
     */
    int wwrite(const char *buffer, int buffLen);

    int in_waiting();

    /** Reads a string of bytes from the serial port.
     *
     * @param buffer pointer to the buffer to be written to
     * @param buffLen the size of the buffer
     * @param nullTerminate if set to true it will null terminate the string
     *
     * @return int the number of bytes read
     */
    int rread(char *buffer, int buffLen, bool nullTerminate = true);

    /** Flushes everything from the serial port's read buffer
     */
    void flush();
};

